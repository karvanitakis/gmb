﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMB.Models
{
    public class GmbAuthTokenRequest
    {
        public string GrantType { get; set; } = "authorization_code";
        public string RedirectUri { get; set; }
    }
}
