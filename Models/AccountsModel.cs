﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMB.Models
{
    public class State
    {
        public string status { get; set; }
        public string vettedStatus { get; set; }
    }

    public class PostalAddress
    {
        public string regionCode { get; set; }
        public string postalCode { get; set; }
        public string locality { get; set; }
        public List<string> addressLines { get; set; }
    }

    public class OrganizationInfo
    {
        public string registeredDomain { get; set; }
        public PostalAddress postalAddress { get; set; }
        public string phoneNumber { get; set; }
    }

    public class Account
    {
        public string name { get; set; }
        public string accountName { get; set; }
        public string type { get; set; }
        public State state { get; set; }
        public string profilePhotoUrl { get; set; }
        public string role { get; set; }
        public string permissionLevel { get; set; }
        public string accountNumber { get; set; }
        public OrganizationInfo organizationInfo { get; set; }
    }

    public class AccountsModel
    {
        public List<Account> accounts { get; set; }
    }
}
