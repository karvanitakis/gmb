﻿using System.Collections.Generic;

namespace GMB.Models
{
    public class LocationsModel
    {
        public List<Location> locations { get; set; }
    }

    public class PrimaryCategory
    {
        public string displayName { get; set; }
        public string categoryId { get; set; }
    }

    public class PlaceInfo
    {
        public string name { get; set; }
        public string placeId { get; set; }
    }

    public class Places
    {
        public List<PlaceInfo> placeInfos { get; set; }
    }

    public class ServiceArea
    {
        public string businessType { get; set; }
        public Places places { get; set; }
    }

    public class LocationKey
    {
        public string placeId { get; set; }
        public string requestId { get; set; }
    }

    public class OpenInfo
    {
        public string status { get; set; }
        public bool canReopen { get; set; }
    }

    public class LocationState
    {
        public bool isGoogleUpdated { get; set; }
        public bool canUpdate { get; set; }
        public bool canDelete { get; set; }
        public bool isVerified { get; set; }
        public bool isPendingReview { get; set; }
    }

    public class Metadata
    {
        public string mapsUrl { get; set; }
        public string newReviewUrl { get; set; }
    }

    public class Address
    {
        public string regionCode { get; set; }
        public string postalCode { get; set; }
        public string locality { get; set; }
        public List<string> addressLines { get; set; }
    }

    public class Location
    {
        public string name { get; set; }
        public string locationName { get; set; }
        public string primaryPhone { get; set; }
        public PrimaryCategory primaryCategory { get; set; }
        public string websiteUrl { get; set; }
        public ServiceArea serviceArea { get; set; }
        public LocationKey locationKey { get; set; }
        public OpenInfo openInfo { get; set; }
        public LocationState locationState { get; set; }
        public Metadata metadata { get; set; }
        public string languageCode { get; set; }
        public Address address { get; set; }
    }
}
