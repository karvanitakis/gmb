﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMB.Models
{
    public class GmbAuthRedirectionRequest
    {
        public string Scope { get; set; }
        public string ResponseType { get; set; }
        public string RedirectUri { get; set; }
        public string AccessType { get; set; }
    }
}
