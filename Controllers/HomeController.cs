﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GMB.Models;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using RestSharp;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace GMB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RequestMyBusinessAccess()
        {
            var model = new GmbAuthRedirectionRequest();

            model.Scope = "https://www.googleapis.com/auth/plus.business.manage";
            model.RedirectUri = Constants.GmbRedirectUri;
            model.ResponseType = "code";
            model.AccessType = "offline";
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Constants.GmbAuthRedirectionUrl}?");
            sb.Append($"scope={model.Scope}&");
            sb.Append($"response_type={model.ResponseType}&");
            sb.Append($"redirect_uri={model.RedirectUri}&");
            sb.Append($"access_type={model.AccessType}&");
            sb.Append($"client_id={Constants.ClientId}");

            return Redirect(sb.ToString());
        }

        public IActionResult ManageGoogleAuthResponse([FromUri] string code, string scope)
        {
            var model = new GmbAuthTokenRequest();

            var client = new RestClient(Constants.GmbRequestTokenUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("code", code);
            request.AddParameter("client_id", Constants.ClientId);
            request.AddParameter("client_secret", Constants.ClientSecret);
            request.AddParameter("redirect_uri", Constants.GmbRedirectUri);
            request.AddParameter("grant_type", model.GrantType);
            IRestResponse response = client.Execute(request);


            var responseDeserialized =  JsonConvert.DeserializeObject<GmbAuthTokenResponse>(response.Content);

            if (string.IsNullOrEmpty(HttpContext.Session.GetString("Access_Token")))
            {
                HttpContext.Session.SetString("Access_Token", responseDeserialized.access_token);
            }
            if (!string.IsNullOrWhiteSpace(responseDeserialized.refresh_token) && string.IsNullOrEmpty(HttpContext.Session.GetString("Refresh_Token")))
            {
                HttpContext.Session.SetString("Refresh_Token", responseDeserialized.refresh_token);
            }

            return RedirectToAction("Index");
        }

        public IActionResult RefreshToken()
        {

            //model.TokenRefresh.grant_type = "refresh_token";

            //var client = new RestClient(model.RequestTokenUrl);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddParameter("client_id", model.ClientId);
            //request.AddParameter("client_secret", model.ClientSecret);
            //request.AddParameter("refresh_token", model.AuthorizationTokensResponse.refresh_token);
            //request.AddParameter("grant_type", model.TokenRefresh.grant_type);
            //IRestResponse response = client.Execute(request);

            //model.AuthorizationTokensResponse = JsonConvert.DeserializeObject<AuthorizationTokensResponse>(response.Content);

            return View("Index");
        }

        public IActionResult RevokeToken()
        {
            //var client = new RestClient(model.RevokeTokenUrl);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.GET);
            //request.AddParameter("token", model.AuthorizationTokensResponse.access_token);

            //IRestResponse response = client.Execute(request);

            return View("Index");
        }

        public IActionResult GetAccounts()
        {
            var bearerToken = HttpContext.Session.GetString("Access_Token");
            var client = new RestClient("https://mybusiness.googleapis.com/v4/accounts");
            client.Timeout = -1;
            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", bearerToken));
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return View("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
