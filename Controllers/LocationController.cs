﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GMB.Models;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using RestSharp;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace GMB.Controllers
{
    public class LocationController : Controller
    {
        private readonly ILogger<LocationController> _logger;

        public LocationController(ILogger<LocationController> logger)
        {
            _logger = logger;
        }

        public IActionResult List(string accountName)
        {
            var locations = GetLocationsForAccount(accountName);
            return View(locations);
        }

        public LocationsModel GetLocationsForAccount(string accountName)
        {
            var bearerToken = HttpContext.Session.GetString("Access_Token");
            var client = new RestClient($"https://mybusiness.googleapis.com/v4/{accountName}/locations");
            client.Timeout = -1;
            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", bearerToken));
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            return JsonConvert.DeserializeObject<LocationsModel>(response.Content);
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        public IActionResult UpdateLocation(Location location)
        {
            var bearerToken = HttpContext.Session.GetString("Access_Token");
            var client = new RestClient($"https://mybusiness.googleapis.com/v4/{location.name}");
            client.Timeout = -1;
            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", bearerToken));
            var request = new RestRequest(Method.PATCH);
            request.AddParameter("updateMask", "primaryPhone");
            request.AddJsonBody(JsonConvert.SerializeObject(location));
            IRestResponse response = client.Execute(request);

            return View("");
        }
    }
}
