﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GMB.Models;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using RestSharp;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace GMB.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger)
        {
            _logger = logger;
        }

        public IActionResult AccountList()
        {
            var bearerToken = HttpContext.Session.GetString("Access_Token");
            var client = new RestClient("https://mybusiness.googleapis.com/v4/accounts");
            client.Timeout = -1;
            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", bearerToken));
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            var accounts = JsonConvert.DeserializeObject<AccountsModel>(response.Content);

            return View(accounts);
        }
    }
}
